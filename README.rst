eselect-wine
============

Manage active Wine slots and variants.

Example::

    $ eselect wine list
    Available Wine slots:
      [1]   wine-proton-7.0.4 *

      [2]   wine-staging-7.19
      [3]   wine-staging-7.20 * (main)
      [4]   wine-staging-7.21

      [5]   wine-vanilla-7.0 *
      [6]   wine-vanilla-7.21

    $ wine --version
    wine-7.20 (Staging)

    $ wine-vanilla --version
    wine-7.0

Makes uses of ``/etc/eselect/wine``, ``PATH``, ``MANPATH``, and
``XDG_DATA_DIRS``. No files in ``/usr`` are modified by this module
since version >=2.0.0. *After* migration and unlike older eselect,
losing data from ``/etc/eselect/wine`` is not harmful (can be
regenerated anytime with ``eselect wine update``).

Using expects the following install layout (``lib64`` is unsupported)::

    /usr/lib/wine-<variant>-<version>/wine/
    /usr/lib/wine-<variant>-<version>/bin/* (must have 'wine')
    /usr/bin/*-<variant>-<version> (wrappers to call the above)
    /usr/include/wine-<variant>-<version>
    /usr/share/wine-<variant>-<version>/{applications,man}

In ebuilds, note ``(de)register`` or using ``pkg_prerm`` is deprecated, use::

    IDEPEND=">=app-eselect/eselect-wine-2"
    pkg_postinst() {
        eselect wine update --if-unset || die
    }
    pkg_postrm() {
        eselect wine update --if-unset || die
    }

Any arbitrary ``wine-myvariant`` is supported for custom ebuilds, except that
the variant name should not contain hyphens (``-``).
